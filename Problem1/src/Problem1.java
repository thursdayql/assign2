
//********************************************************************
//File:         Problem1.java       
//Author:       Liam Quinn
//Date:         September 27th 2017
//Course:       CPS100
//
//Problem Statement:
// Write an application that generates a random phone number
// with certain restrictions.
//
// Inputs:  None
// Outputs: Phone Number in the form of XXX-XXX-XXX with restrictions.
// 
//********************************************************************

import java.util.*;


public class Problem1
{

  public static void main(String[] args)
  {
    
    //Defines the first set of integers
    Random generator = new Random();
    int firstNum1 = (generator.nextInt(7) + 1);
    int secondNum1 = (generator.nextInt(7) + 1);
    int thirdNum1 = (generator.nextInt(7) + 1);
    String firstSet = "" + firstNum1 + secondNum1 + thirdNum1;

    //Defines the seconds set of integers
    int firstNum2 = (generator.nextInt(6) + 1);
    int secondNum2 = (generator.nextInt(5) + 1);
    int thirdNum2 = (generator.nextInt(5) + 1);
    String secondSet = "" + firstNum2 + secondNum2 + thirdNum2;

    //Defines the first set of integers
    int firstNum3 = (generator.nextInt(9) + 1);
    int secondNum3 = (generator.nextInt(9) + 1);
    int thirdNum3 = (generator.nextInt(9) + 1);
    int fourthNum3 = (generator.nextInt(9) + 1);
    String thirdSet = "" + firstNum3 + secondNum3 + thirdNum3 + fourthNum3;

    //Outputs the created phone number
    System.out.println("Phone Number = " + firstSet + ("-") + secondSet + ("-")
                       + thirdSet);


  }

}
