
//********************************************************************
//File:         Problem2.java       
//Author:       Liam Quinn
//Date:         September 27th 2017
//Course:       CPS100
//
//Problem Statement:
// Write a program that reads the radius of a sphere and prints its
// volume and surface area.
//
// Inputs:  Radius.
// Outputs: Volume and surface area.
// 
//********************************************************************

import java.text.DecimalFormat;
import java.util.*;


public class Problem2
{

  public static void main(String[] args)
  {
    Scanner input = new Scanner(System.in);

    //Stores integer in radius variable from keyboard input
    double radius = 0;
    System.out.println("Give Radius: ");
    radius = input.nextDouble();

    //Does math to produce the volume and surfaceArea
    double volume = (1.3333 * 3.1415 * Math.pow(radius, 3));
    double surfaceArea = (4 * 3.1415 * Math.pow(radius, 2));

    //Creates formatter and formats volume and surfaceArea
    DecimalFormat format = new DecimalFormat("#.####");
    format.format(volume);
    format.format(surfaceArea);

    //Outputs both volume and surfaceArea
    System.out.println("Volume of Sphere = " + format.format(volume));
    System.out
        .println("Surface Area of Sphere = " + format.format(surfaceArea));


    input.close();
  }

}
